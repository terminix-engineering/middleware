package middleware

import (
	"net/http"
	"os"

	"github.com/gorilla/handlers"
)

//LoggingMiddleware applies logging
type LoggingMiddleware struct {
	output *os.File
}

//SetOutput accepts an io.writer for log output
func (lmw *LoggingMiddleware) SetOutput(writer *os.File) {
	lmw.output = writer
}

//Middleware is the handler for logging middleware
func (lmw *LoggingMiddleware) Middleware(next http.Handler) http.Handler {
	//make sure the logger output is not nil
	if lmw.output == nil {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			http.Error(w, "No Output Defined for Logging", http.StatusInternalServerError)
			return
		})
	}
	return handlers.CombinedLoggingHandler(lmw.output, next)
}
