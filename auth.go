package middleware

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

type openIDKeys struct {
	Keys []struct {
		Kty    string   `json:"kty"`
		Use    string   `json:"use"`
		Kid    string   `json:"kid"`
		X5T    string   `json:"x5t"`
		N      string   `json:"n"`
		E      string   `json:"e"`
		X5C    []string `json:"x5c"`
		Issuer string   `json:"issuer"`
	} `json:"keys"`
}

type openIDConfig struct {
	TokenEndpoint                     string      `json:"token_endpoint"`
	TokenEndpointAuthMethodsSupported []string    `json:"token_endpoint_auth_methods_supported"`
	JwksURI                           string      `json:"jwks_uri"`
	ResponseModesSupported            []string    `json:"response_modes_supported"`
	SubjectTypesSupported             []string    `json:"subject_types_supported"`
	IDTokenSigningAlgValuesSupported  []string    `json:"id_token_signing_alg_values_supported"`
	ResponseTypesSupported            []string    `json:"response_types_supported"`
	ScopesSupported                   []string    `json:"scopes_supported"`
	Issuer                            string      `json:"issuer"`
	RequestURIParameterSupported      bool        `json:"request_uri_parameter_supported"`
	UserinfoEndpoint                  string      `json:"userinfo_endpoint"`
	AuthorizationEndpoint             string      `json:"authorization_endpoint"`
	HTTPLogoutSupported               bool        `json:"http_logout_supported"`
	FrontchannelLogoutSupported       bool        `json:"frontchannel_logout_supported"`
	EndSessionEndpoint                string      `json:"end_session_endpoint"`
	ClaimsSupported                   []string    `json:"claims_supported"`
	TenantRegionScope                 interface{} `json:"tenant_region_scope"`
	CloudInstanceName                 string      `json:"cloud_instance_name"`
	CloudGraphHostName                string      `json:"cloud_graph_host_name"`
	MsgraphHost                       string      `json:"msgraph_host"`
	RbacURL                           string      `json:"rbac_url"`
}

//Claims is the object model of claims
type Claims struct {
	Scp string `json:"scp"`
	jwt.StandardClaims
}

const (
	noTokenErr         = "bearer token required"
	tokenParseErr      = "could not parse bearer token"
	scopeNotAuthorized = "the scope of the bearer token is not authorized"
	userNotDefined     = "the user identifier was not defined in the token"
)

//AuthMiddleWare is the struct that contains the auth scope
type AuthMiddleWare struct {
	authScope       map[string]string
	bypassRoutes    map[string]bool
	openIDConfigURL string
}

//SetAuthScopes sets auth scopes
func (amw *AuthMiddleWare) SetAuthScopes(scopes map[string]string) {
	amw.authScope = scopes
}

//SetAuthBypass allows the server to specify a bypass on some routes
func (amw *AuthMiddleWare) SetAuthBypass(bypassRoutes map[string]bool) {
	amw.bypassRoutes = bypassRoutes
}

//SetOpenIDConfigURL sets the endpoint for open id connect
func (amw *AuthMiddleWare) SetOpenIDConfigURL(url string) {
	amw.openIDConfigURL = url
}

//Middleware is the handler that checks validation.
func (amw *AuthMiddleWare) Middleware(next http.Handler) http.Handler {
	//make sure we've got the values we need for authorization
	if len(amw.authScope) == 0 || amw.openIDConfigURL == "" {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			http.Error(w, "AuthMiddleware is applied, but not configured. OpenID URL and Auth Scope are mandatory", http.StatusInternalServerError)
			return
		})
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		//validate settings have been applied

		routeName := mux.CurrentRoute(r).GetName()
		//check bypass routes
		if amw.bypassRoutes[routeName] == true {
			next.ServeHTTP(w, r)
		} else {

			err := isAuthorized(w, r, routeName, amw)

			if err != nil {
				log.Debugf("failed to authorize bearer token: %v", err.Error())
				http.Error(w, err.Error(), http.StatusUnauthorized)
			} else {
				next.ServeHTTP(w, r)
			}
		}
	})
}

func isAuthorized(w http.ResponseWriter, r *http.Request, routeName string, amw *AuthMiddleWare) (err error) {
	var token = r.Header.Get("Authorization")
	if token == "" {
		log.Infof("No Token")
		return fmt.Errorf(noTokenErr)
	}

	chunks := strings.SplitN(token, " ", 2)
	if len(chunks) == 2 {
		token = chunks[1]
	}

	claims := &Claims{}
	tkn, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {

		conf, err := getPublicOpenIDConfig(amw.openIDConfigURL)
		if err != nil {
			log.Error(err)
			return nil, err
		}
		signingKey, err := getPublicSigningStringFromConfig(conf, fmt.Sprintf("%v", token.Header["kid"]))
		if err != nil {
			log.Error(err)
			return nil, err
		}
		block, _ := pem.Decode([]byte(signingKey))
		var cert *x509.Certificate
		cert, _ = x509.ParseCertificate(block.Bytes)
		pub := cert.PublicKey.(*rsa.PublicKey)

		//check scopes
		for _, scope := range strings.Split(claims.Scp, " ") {
			if scope == amw.authScope[routeName] {
				//they've got the right scope, send em out of the loop
				return pub, nil
			}
		}
		return nil, errors.New("Unauthorized Scope")
	})

	if err != nil {
		//w.WriteHeader(http.StatusUnauthorized)
		return errors.New("Authorization Token Invalid")
	}
	if !tkn.Valid {
		//w.WriteHeader(http.StatusUnauthorized)
		return errors.New("Invalid Token")
	}

	return nil

}

func getPublicOpenIDConfig(OpenIDConfigURL string) (config openIDConfig, err error) {

	request, _ := http.NewRequest("GET", OpenIDConfigURL, nil)
	client := &http.Client{}
	//execute
	response, err := client.Do(request)
	if err != nil {
		log.Error(err)
		return config, err
	}
	if response.StatusCode > 299 {
		log.Error(response.StatusCode)
	}
	defer response.Body.Close()

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Error(err)
	}
	//read data
	json.Unmarshal(data, &config)
	return config, err
}

func getPublicSigningStringFromConfig(config openIDConfig, keyID string) (signingKey string, err error) {

	configURL := config.JwksURI

	request, _ := http.NewRequest("GET", configURL, nil)
	client := &http.Client{}
	//execute
	response, err := client.Do(request)
	if err != nil {
		log.Error(err)
		return "", err
	}
	if response.StatusCode > 299 {
		log.Error(response.StatusCode)
		return "", err
	}
	defer response.Body.Close()

	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Error(err)
		return "", err
	}
	keySet := openIDKeys{}
	if err != nil {
		log.Error(err)
		return "", err
	}
	err = json.Unmarshal(data, &keySet)

	for _, key := range keySet.Keys {
		if key.Kid == keyID {
			return fmt.Sprintf("%s%s%s",
				"-----BEGIN CERTIFICATE-----\n",
				key.X5C[0],
				"\n-----END CERTIFICATE-----"), nil
		}
	}
	err = errors.New("Unable to find matching key")
	log.Error(err)
	return "", err
}
