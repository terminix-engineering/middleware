package middleware

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

//ValidatorMiddleWare validates the content typs of a request
type ValidatorMiddleWare struct {
	AcceptedContent map[string][]string
}

//SetAcceptedContent actually sets the acceptec content for an http post
func (vmw *ValidatorMiddleWare) SetAcceptedContent(accepted map[string][]string) {
	vmw.AcceptedContent = accepted
}

//Middleware is the handler for content type validation
func (vmw *ValidatorMiddleWare) Middleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		routeName := mux.CurrentRoute(r).GetName()

		success := validate(r, vmw.AcceptedContent[routeName])
		if !success {
			http.Error(w, "content type not accepted", http.StatusNotAcceptable)
			return
		}

		next.ServeHTTP(w, r)
	})
}

func validate(r *http.Request, acceptedContent []string) (success bool) {

	if findStringInSlice(r.Header.Get("content-type"), acceptedContent) {
		return true
	}

	return false
}

func findStringInSlice(searchText string, slice []string) bool {
	for _, foundText := range slice {
		if strings.ToLower(searchText) == strings.ToLower(foundText) {
			return true
		}
	}
	return false
}
