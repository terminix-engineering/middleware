# middleware

  

> This is a shared middleware library for api development based on gorilla/mux and dgrijalva/jwt-go.

![Build Status](http://img.shields.io/travis/badges/badgerbadgerbadger.svg?style=flat-square)

-  [Installation](#installation)
	- `go get gitlab.com/terminix/middleware`

-  [Features](#features)
	- JWT Authorization that works with Azure Active Directory leveraging https://github.com/dgrijalva/jwt-go for JWT tasks
	- Support for scope validations from JWT (* Scope is pulled from "scp" claim)
	- Assignable scope per named route
	- Content type validations per named route
	- GZip compression leveraging https://github.com/gorilla/mux 's `handler.CompressHandler`
	- Apache Standard log formatting leveraging https://github.com/gorilla/mux 's `handler.CombinedLoggingHandler`
- [External Packages/Dependencies](#dependencies)
    - 	https://github.com/dgrijalva/jwt-go
    - 	https://github.com/gorilla/mux
    - 	https://github.com/sirupsen/logrus
   
---

## Authorization Example
```go
//be sure and name your MUX routes
r.Handle("/hello", appHandler{config, HandleHello}).Methods(http.MethodPost).Name("Hello")
r.Handle("/goodbye", appHandler{config, HandleHello}).Methods(http.MethodPost).Name("Goodbye")

//define scopes for auth validation
const (
	writeScope =    "Hello.Write"
	readScope =     "Hello.Read"
)
//define a map table of scopes for each route.
routeAuthScopes = map[string]string{
	"Hello":    writeScope,
	"Goodbye":  readScope,
}
//define any routes that should not be authorized (such as healthcheck)
// a true value in the map will bypass auth
routeBypassScopes = map[string]bool{
	"Hello":        false,
	"Goodbye":      false,
	"HealthCheck":  true,
}

//Apply the settings
//use authorization middleware
amw := middleware.AuthMiddleWare{}
amw.SetAuthScopes(routeAuthScopes)
amw.SetAuthBypass(routeBypassScopes)
amw.SetOpenIDConfigURL("https://login.microsoft.com/....")
r.Use(amw.Middleware)
```


## Content Type Validation Example
```go
//be sure and name your MUX routes
r.Handle("/hello", appHandler{config, HandleHello}).Methods(http.MethodPost).Name("Hello")
r.Handle("/goodbye", appHandler{config, HandleHello}).Methods(http.MethodPost).Name("Goodbye")
//define slice of accepted content types. 
contentTypeHello = []string{"application/json", "application/protobuf"}
contentTypeGoodbye = []string{"application/json"}
//create a map to hold the type for each route
routeAcceptedContents = map[string][]string{
	"Hello": contentTypeHello ,
	"Goodbye": contentTypeGoodbye ,
}

//Apply the settings
vmw := middleware.ValidatorMiddleWare{}
vmw.SetAcceptedContent(routeAcceptedContents)
//use validation middleware
r.Use(vmw.Middleware)
```


## Logging Example
```go
lmw := middleware.LoggingMiddleware{}
lmw.SetOutput(os.Stdout)//output accepts *os.File
r.Use(lmw.Middleware)
```

## Compression Example
```go
//set up compression for responses
cmw := middleware.CompressionMiddleware{}
r.Use(cmw.Middleware)
```